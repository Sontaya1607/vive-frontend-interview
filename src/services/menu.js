import http from '@/libs/http'

export default {
  findAll () {
    return http.get('/menu')
      .then(response => response.data)
  }
}
