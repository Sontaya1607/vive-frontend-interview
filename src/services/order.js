import http from '@/libs/http'

export default {
  findAll () {
    return http.get('/order')
      .then(response => response.data)
  },
  addToAPI (userCheckout) {
    return http.post('/order', userCheckout)
      .then((response) => {
        console.log(response)
      })
      .catch((error) => {
        console.log(error)
      })
  }
}
